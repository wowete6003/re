docker run -d -v /workspace/re/mc:/data \
    -e TYPE=SPIGOT \
    -e MODE=creative \
    -e DIFFICULTY=peaceful \
    -e ONLINE_MODE=false \
    -e ALLOW_FLIGHT=true \
    -e VERSION=1.12.2 \
    -e MEMORY=8G \
    -p 25565:25565 -e EULA=TRUE --name mc itzg/minecraft-server